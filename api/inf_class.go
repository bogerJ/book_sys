// @author a
// @date   
// @note   
package api

import (
    "bookSys/global/response"
    "bookSys/model"
    "github.com/gin-gonic/gin"
)

// @Router /class/add [post]
func AddInfClass(c *gin.Context) {

    var book  model.InfClass
    _=c.ShouldBindJSON(&book)
    if book.Save(){
        response.OkWithMessage("添加成功",c)
    }else{
        response.FailWithMessage("添加失败",c)
    }
}

// @Router /class/list [post]
func InfClassList(c *gin.Context) {
    teaching:=model.InfClass{}
    _=c.ShouldBindJSON(&teaching)
    list := teaching.PageList()
    response.OkWithData(list,c)
}

// @Router /class/list [post]
func InfClassUpdate(c *gin.Context) {
    teaching:=model.InfClass{}
    _=c.ShouldBindJSON(&teaching)
    list := teaching.UpDate()
    response.OkWithData(list,c)
}

