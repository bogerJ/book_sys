// @author a
// @date   
// @note   
package api

import (
    "bookSys/global/response"
    "bookSys/model"
    "github.com/gin-gonic/gin"
)

// @Router /grade/add [post]
func AddInfGrade(c *gin.Context) {

    var book  model.InfGrade
    _=c.ShouldBindJSON(&book)
    if book.Save(){
        response.OkWithMessage("添加成功",c)
    }else{
        response.FailWithMessage("添加失败",c)
    }
}

// @Router /grade/list [post]
func InfGradeList(c *gin.Context) {
    teaching:=model.InfGrade{}
    _=c.ShouldBindJSON(&teaching)
    list := teaching.PageList()
    response.OkWithData(list,c)
}

// @Router /grade/list [post]
func InfGradeUpdate(c *gin.Context) {
    teaching:=model.InfGrade{}
    _=c.ShouldBindJSON(&teaching)
    list := teaching.UpDate()
    response.OkWithData(list,c)
}

