// @author a
// @date   
// @note   
package api

import (
    "bookSys/global/response"
    "bookSys/model"
    "github.com/gin-gonic/gin"
)

// @Router /subject/add [post]
func AddInfSubject(c *gin.Context) {

    var book  model.InfSubject
    _=c.ShouldBindJSON(&book)
    if book.Save(){
        response.OkWithMessage("添加成功",c)
    }else{
        response.FailWithMessage("添加失败",c)
    }
}

// @Router /subject/list [post]
func InfSubjectList(c *gin.Context) {
    teaching:=model.InfSubject{}
    _=c.ShouldBindJSON(&teaching)
    list := teaching.PageList()
    response.OkWithData(list,c)
}

// @Router /subject/list [post]
func InfSubjectUpdate(c *gin.Context) {
    teaching:=model.InfSubject{}
    _=c.ShouldBindJSON(&teaching)
    list := teaching.UpDate()
    response.OkWithData(list,c)
}

