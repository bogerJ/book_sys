// @author a
// @date   
// @note   
package api

import (
    "bookSys/global/response"
    "bookSys/model"
    "github.com/gin-gonic/gin"
)

// @Router /book/add [post]
func AddTeachingBook(c *gin.Context) {

    var book  model.TeachingBook
    _=c.ShouldBindJSON(&book)
    if book.Save(){
        response.OkWithMessage("添加成功",c)
    }else{
        response.FailWithMessage("添加失败",c)
    }

}

// @Router /book/list [post]
func TeachingBookList(c *gin.Context) {
    teaching:=model.TeachingBook{}
    _=c.ShouldBindJSON(&teaching)
    list := teaching.PageList()
    response.OkWithData(list,c)
}

// @Router /book/list [post]
func TeachingBookUpdate(c *gin.Context) {
    teaching:=model.TeachingBook{}
    _=c.ShouldBindJSON(&teaching)
    list := teaching.UpDate()
    response.OkWithData(list,c)
}

