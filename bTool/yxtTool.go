package bTool

import (
	"bookSys/static"
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"encoding/hex"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

/**
获取本地ip
*/
func GetLoaclIp() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "localhost"
	}
	for _, addrss := range addrs {
		if ipnet, ok := addrss.(*net.IPNet); ok && !ipnet.IP.IsLoopback() && !ipnet.IP.IsLinkLocalUnicast() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return "localhost"
}

/**
数组转16 进制字符串
*/
func ByteToHexString(b []byte) string {
	var hexStr = ""
	for _, i := range b {
		s := strconv.FormatInt(int64(i&0xff), 16)
		if len(s) < 2 {
			s = MergeString("0", s)
		}
		hexStr = MergeString(hexStr, s)
		hexStr = strings.ToUpper(hexStr)
	}
	return hexStr
}

/**
数组转BCD码
*/
func ByteToBCDString(b []byte) string {
	var hexStr = ""
	for _, i := range b {
		s := strconv.FormatInt(int64(i), 16)
		if len(s) < 2 {
			s = MergeString("0", s)
		}
		hexStr = MergeString(hexStr, s)

		hexStr = strings.ToUpper(hexStr)
	}
	return hexStr
}

/**
数组合并
*/
func Marge(b1 []byte, b2 []byte) (b []byte) {
	count := len(b1) + len(b2)
	b = make([]byte, count)
	index := copy(b, b1)
	copy(b[index:], b2)
	return

}

func MergeString(strs ...string) string {
	b := bytes.NewBufferString("")
	for _, value := range strs {
		b.WriteString(value)
	}
	return b.String()
}

/*
两个字节 转 uint16
*/
func GetUint16(bytes []byte) uint16 {
	return (uint16(bytes[0]) << 8) | uint16(bytes[1])
}

/*
四个字节 转 uint32
*/
func GetUint32(bytes []byte) uint32 {
	return (uint32(bytes[0]) << 24) | (uint32(bytes[1]) << 16) | (uint32(bytes[2]) << 8) | uint32(bytes[3])
}

// byte 转化 int
func GetInt64(by []byte, num *int64)  {
	b_buf := bytes.NewBuffer(by)
	binary.Read(b_buf, binary.LittleEndian, num)
}
// byte 转化 int
func GetInt32(by []byte, num *int32)  {
	b_buf := bytes.NewBuffer(by)
	binary.Read(b_buf, binary.LittleEndian, num)
}
// byte 转化 int
func GetInt16(by []byte, num *int16)  {
	b_buf := bytes.NewBuffer(by)
	binary.Read(b_buf, binary.LittleEndian, num)
}
// 数字 转化 byte
func GetBytes(num *int32) []byte {
	b_buf := new(bytes.Buffer)
	binary.Write(b_buf, binary.LittleEndian,num)
	return b_buf.Bytes()
}


func HexStringToByte(hexStr string) (bs []byte) {
	s_len := len(hexStr)
	bs = make([]byte, s_len/2)
	if s_len%2 == 0 {
		for i := 0; i < s_len/2; i++ {
			hex := hexStr[2*i : 2*i+2]
			b, _ := strconv.ParseInt(hex, 16, 32)
			bs[i] = byte(b & 0xff)
		}
	} else {
		bs = nil
	}

	return

}

func FormatUtcTime(bytes []byte) string {
	utcTime := GetUint32(bytes)
	d := time.Unix(int64(utcTime), 0)
	return d.Format(static.TimeLayOut1)
}

func ParseLonglan(bytes []byte) float64 {
	du := bytes[0]
	fen := float64(bytes[1]) + float64(bytes[2])/100 + float64(bytes[3])/10000
	return float64(du) + fen/60

}

func FloatToString(f float64, n int) string {
	return strconv.FormatFloat(f, 'f', n, 64)

}
func StringToFloat(s string) float64 {
	f, _ := strconv.ParseFloat(s, 64)
	return f
}
func FormatFloat(f float64, n int) float64 {
	fStr := FloatToString(f, n)
	return StringToFloat(fStr)

}
// ClientIP 尽最大努力实现获取客户端 IP 的算法。
// 解析 X-Real-IP 和 X-Forwarded-For 以便于反向代理（nginx 或 haproxy）可以正常工作。
func ClientIP(r *http.Request) string {
	xForwardedFor := r.Header.Get("X-Forwarded-For")
	ip := strings.TrimSpace(strings.Split(xForwardedFor, ",")[0])
	if ip != "" {
		return ip
	}

	ip = strings.TrimSpace(r.Header.Get("X-Real-Ip"))
	if ip != "" {
		return ip
	}

	if ip, _, err := net.SplitHostPort(strings.TrimSpace(r.RemoteAddr)); err == nil {
		return ip
	}

	return ""
}
func  GetRandomString(l int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyz"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < l; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}
func MD5Encrypt(str string) string  {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}
func IsExist(f string) bool {
	_, err := os.Stat(f)
	return err == nil || os.IsExist(err)
}
func IsFile(f string) bool {
	fi, e := os.Stat(f)
	if e != nil {
		return false
	}
	return !fi.IsDir()
}
//截取扩展名
func SubExt(filePath string) string {
	index:=strings.LastIndex(filePath,".")
	return filePath[index:]
}

func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}
