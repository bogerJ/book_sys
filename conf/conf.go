package conf

import (
	"fmt"
	"gopkg.in/gcfg.v1"
)

type Conf struct {
	Server Server
	Db1 Db1
	Db2 Db2
	Tixian Tixian
}
type Server struct {
	Port string
}
type Db1 struct {
	Url string
}
type Db2 struct {
	Url string
}
type Tixian struct {
	Mtype string
}

type Log struct {
	Prefix  string `mapstructure:"prefix" json:"prefix" yaml:"prefix"`
	LogFile bool   `mapstructure:"log-file" json:"logFile" yaml:"log-file"`
	Stdout  string `mapstructure:"stdout" json:"stdout" yaml:"stdout"`
	File    string `mapstructure:"file" json:"file" yaml:"file"`
}

var  CONF=&Conf{}

func LoadConfig(path string)  {
	conf:=Conf{}
	err := gcfg.ReadFileInto(&conf, path)
	if err != nil {
		fmt.Println("Failed to parse config file: %s", err)
	}
	CONF=&conf
}