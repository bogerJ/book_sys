package main

import (
	"bookSys/conf"
	"bookSys/router"
	"bookSys/sqlUtil"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
)

func init() {
	conf.LoadConfig("conf/my.ini")
	//staticHandler = http.FileServer(http.Dir(dir))
}
func main() {
	sqlUtil.InitAccess()
	Router := Routers()

	s := &http.Server{
		Addr:         conf.CONF.Server.Port,
		Handler:      Router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
		//WriteTimeout:   15 * time.Minute,
		MaxHeaderBytes: 1 << 20,
	}
	if err := s.ListenAndServe();err!=nil{
		log.Fatal(err)
	}
}

func Routers() *gin.Engine {
	var Router = gin.Default()
	ApiGroup := Router.Group("/admin")

	router.InitRouter(ApiGroup)
	log.Println("router register success")
	return Router
}