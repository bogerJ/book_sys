package model

import (
	"bookSys/global"
	"bookSys/sqlUtil"
	"strconv"
)

type InfClass struct {
	Id          int    `json:"id"`
	ClassCode string `json:"classCode"`
	ClassName    string `json:"className"`
	DeletedAt    string `json:"deletedAt"`
}

func (InfClass) TableName() string {
	return "inf_class"
}

func (b *InfClass) Save() bool {

	insertSql := "insert into inf_class (class_code,class_name) values (?,?)"

	succ := sqlUtil.Insert(insertSql, b.ClassCode, b.ClassName)
	if !succ {
		return false
	}
	return true
}
func (b *InfClass) UpDate() bool {

	if b.Id == 0 {
		return false
	}

	updateSql := "update inf_class t set t.class_code = ?,t.class_name=? where t.id =?"

	succ := sqlUtil.Update(updateSql, b.ClassCode, b.ClassName, b.Id)
	if !succ {
		return false
	}
	return true
}
func (b *InfClass) PageList() (list []InfClass) {
	list = make([]InfClass,0)

	querySql := "SELECT * from inf_class"

	if b.Id!=0{
		querySql += " where id ="+strconv.Itoa(b.Id)
	}

	rows, err := sqlUtil.QueryRows(querySql)
	if err!=nil{
		global.GVA_LOG.Error(err.Error())
		return list
	}
	defer rows.Close()
	for rows.Next() {
		book := InfClass{}
		deletedAt:=""
		err = rows.Scan(&book.Id,&book.ClassName,&book.ClassCode,&deletedAt)
		if err != nil {
			global.GVA_LOG.Error(err.Error())
			return list
		}
		list = append(list, book)
	}
	return list
}
