package model

import (
	"bookSys/global"
	"bookSys/sqlUtil"
	"strconv"
)

type InfGrade struct {
	Id          int    `json:"id"`
	GradeCode    string `json:"gradeCode"`
	GradeName    string `json:"gradeName"`
	DeletedAt    string `json:"deletedAt"`
}

func (InfGrade) TableName() string {
	return "inf_grade"
}

func (b *InfGrade) Save() bool {

	insertSql := "insert into inf_grade (grade_code,grade_name) values (?,?)"

	succ := sqlUtil.Insert(insertSql, b.GradeCode, b.GradeName)
	if !succ {
		return false
	}
	return true
}
func (b *InfGrade) UpDate() bool {

	if b.Id == 0 {
		return false
	}

	updateSql := "update inf_grade t set t.grade_code = ?,t.grade_name=? where t.id =?"

	succ := sqlUtil.Update(updateSql, b.GradeCode, b.GradeName, b.Id)
	if !succ {
		return false
	}
	return true
}
func (b *InfGrade) PageList() (list []InfGrade) {
	list = make([]InfGrade,0)

	querySql := "SELECT * from inf_grade"

	if b.Id!=0{
		querySql += " where id ="+strconv.Itoa(b.Id)
	}

	rows, err := sqlUtil.QueryRows(querySql)
	if err!=nil{
		global.GVA_LOG.Error(err.Error())
		return list
	}
	defer rows.Close()
	for rows.Next() {
		book := InfGrade{}
		deletedAt:=""
		err = rows.Scan(&book.Id,&book.GradeName,&book.GradeCode,&deletedAt)
		if err != nil {
			global.GVA_LOG.Error(err.Error())
			return list
		}
		list = append(list, book)
	}
	return list
}
