package model

import (
	"bookSys/global"
	"bookSys/sqlUtil"
	"strconv"
)

type InfSubject struct {
	Id          int    `json:"id"`
	SubjectCode  string `json:"subjectCode"`
	SubjectName  string `json:"subjectName"`
	DeletedAt    string `json:"deletedAt"`
}

func (InfSubject) TableName() string {
	return "inf_subject"
}

func (b *InfSubject) Save() bool {

	insertSql := "insert into inf_subject (subject_code,subject_name) values (?,?)"

	succ := sqlUtil.Insert(insertSql, b.SubjectCode, b.SubjectName)
	if !succ {
		return false
	}
	return true
}
func (b *InfSubject) UpDate() bool {

	if b.Id == 0 {
		return false
	}

	updateSql := "update inf_subject t set t.subject_code = ?,t.subject_name=? where t.id =?"

	succ := sqlUtil.Update(updateSql, b.SubjectCode, b.SubjectName, b.Id)
	if !succ {
		return false
	}
	return true
}
func (b *InfSubject) PageList() (list []InfSubject) {
	list = make([]InfSubject,0)

	querySql := "SELECT * from inf_subject"

	if b.Id!=0{
		querySql += " where id ="+strconv.Itoa(b.Id)
	}

	rows, err := sqlUtil.QueryRows(querySql)
	if err!=nil{
		global.GVA_LOG.Error(err.Error())
		return list
	}
	defer rows.Close()
	for rows.Next() {
		book := InfSubject{}
		err = rows.Scan(&book.Id,&book.SubjectCode,&book.SubjectName,&book.DeletedAt)
		if err != nil {
			global.GVA_LOG.Error(err.Error())
			return list
		}
		list = append(list, book)
	}
	return list
}
