package model

import (
	"bookSys/global"
	"bookSys/sqlUtil"
	"fmt"
	"strconv"
)

type TeachingBook struct {
	Id          int    `json:"id"`
	BookCode    string `json:"bookCode"`
	BookName    string `json:"bookName"`
	BookGrade   int    `json:"bookGrade"`
	GradeName   *string `json:"gradeName"`
	BookClass   int    `json:"bookClass"`
	ClassName   *string `json:"className"`
	BookSubject int    `json:"bookSubject"`
	SubjectName *string `json:"subjectName"`
	BookOwner   string `json:"bookOwner"`
}

func (TeachingBook) TableName() string {
	return "teaching_book"
}

func (b *TeachingBook) Save() bool {

	var (
		grade InfGrade
	)

	grade=InfGrade{}
	insertSql := "insert into teaching_book (book_code,book_name,book_grade,book_class,book_subject,book_owner) values (?,?,?,?,?,?)"
	grade.Id=b.BookGrade

	b.BookCode = fmt.Sprintf("%03d%03d%03d",b.BookClass,b.BookSubject,b.BookClass)

	succ := sqlUtil.Insert(insertSql, b.BookCode, b.BookName, b.BookGrade, b.BookClass, b.BookSubject,b.BookOwner)
	if !succ {
		return false
	}
	return true
}
func (b *TeachingBook) QueryById() bool {

	insertSql := "insert into teaching_book (book_code,book_name,book_grade,book_class,book_subject) values (?,?,?,?,?)"

	succ := sqlUtil.Insert(insertSql, b.BookCode, b.BookName, b.BookGrade, b.BookClass, b.BookSubject)
	if !succ {
		return false
	}
	return true
}
func (b *TeachingBook) UpDate() bool {

	if b.Id == 0 {
		return false
	}

	updateSql := "update teaching_book t set t.book_name = ?,t.book_grade=?,t.book_subject=?,t.book_class=?,t.book_code =?,t.book_owner=? where t.id =?"

	b.BookCode = fmt.Sprintf("%03d%03d%03d",b.BookClass,b.BookSubject,b.BookClass)
	succ := sqlUtil.Update(updateSql, b.BookName, b.BookGrade,b.BookSubject, b.BookClass, b.BookCode,b.BookOwner, b.Id)
	if !succ {
		return false
	}
	return true
}
func (b *TeachingBook) PageList() (list []TeachingBook) {
	list = make([]TeachingBook,0)

	querySql := "SELECT " +
		"teaching_book.id, " +
		"teaching_book.book_name, " +
		"teaching_book.book_code,teaching_book.book_grade,teaching_book.book_subject,teaching_book.book_class, " +
		"inf_grade.grade_name, " +
		"inf_subject.subject_name, " +
		"inf_class.class_name FROM " +
		"((teaching_book LEFT JOIN inf_grade ON teaching_book.book_grade = inf_grade.ID) " +
		"LEFT JOIN inf_subject ON teaching_book.book_subject = inf_subject.ID) " +
		"LEFT JOIN inf_class ON teaching_book.book_class = inf_class.ID where 1=1 "

	if b.Id != 0 {
		querySql += " and teaching_book.id ="+strconv.Itoa(b.Id)
	}

	if b.BookCode!=""{
		querySql += " and teaching_book.book_code like '%"+b.BookCode+"'"
	}
	if b.BookName!=""{
		querySql += " and teaching_book.book_name like '%"+b.BookName+"%'"
	}
	if b.BookOwner!=""{
		querySql += " and teaching_book.book_owner like '%"+b.BookOwner+"%'"
	}
	if b.BookGrade!=0{
		querySql += " and teaching_book.book_grade = "+strconv.Itoa(b.BookGrade)
	}
	if b.BookSubject!=0{
		querySql += " and teaching_book.book_subject = "+strconv.Itoa(b.BookSubject)
	}
	if b.BookClass!=0{
		querySql += " and teaching_book.book_class = "+strconv.Itoa(b.BookClass)
	}

	rows, err := sqlUtil.QueryRows(querySql)
	if err!=nil{
		global.GVA_LOG.Error(err.Error())
		return list
	}
	defer rows.Close()
	for rows.Next() {
		book := TeachingBook{}
		err = rows.Scan(&book.Id,&book.BookName,&book.BookCode,&book.BookGrade,&book.BookSubject,&book.BookClass ,&book.GradeName,&book.SubjectName,&book.ClassName)
		if err != nil {
			global.GVA_LOG.Error(err.Error())
			return list
		}
		list = append(list, book)
	}
	return list
}
