package router

import (
    "bookSys/api"
    "github.com/gin-gonic/gin"
)

func InitRouter(Router *gin.RouterGroup) {
    BookRouter := Router.Group("book")
    {
        BookRouter.POST("add",api.AddTeachingBook )       // 添加/编辑活动
        BookRouter.POST("list", api.TeachingBookList) // 获取活动列表
        BookRouter.POST("update", api.TeachingBookUpdate) // 获取活动列表
    }

    GradeRouter := Router.Group("grade")
    {
        GradeRouter.POST("add",api.AddInfGrade )       // 添加/编辑活动
        GradeRouter.POST("list", api.InfGradeList) // 获取活动列表
        GradeRouter.POST("update", api.InfGradeUpdate) // 获取活动列表
    }

    SubjectRouter := Router.Group("subject")
    {
        SubjectRouter.POST("add",api.AddInfSubject )       // 添加/编辑活动
        SubjectRouter.POST("list", api.InfSubjectList) // 获取活动列表
        SubjectRouter.POST("update", api.InfSubjectUpdate) // 获取活动列表
    }
    ClassRouter := Router.Group("class")
    {
        ClassRouter.POST("add",api.AddInfClass )       // 添加/编辑活动
        ClassRouter.POST("list", api.InfClassList) // 获取活动列表
        ClassRouter.POST("update", api.InfClassUpdate) // 获取活动列表
    }
}
