package static

// iota 初始化后会自动递增
var codesMap = make(map[string]string)

func init() {

}

func Get(cName string) string {
	value, ok := codesMap[cName]
	if ok {
		return value
	} else {
		return ""
	}
}
